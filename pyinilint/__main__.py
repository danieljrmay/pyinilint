"""Entry point for pyinilint."""

from pyinilint.pyinilint import main

if __name__ == "__main__":
    main()
