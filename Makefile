#!/usr/bin/make -f
#
# pyinilint GNU Makefile
#
# See: https://www.gnu.org/software/make/manual
#
# Copyright (c) 2019 Daniel J. R. May
#

# Makefile command variables
BZIP2=/usr/bin/bzip2
DNF=/usr/bin/dnf
DNF_COPR=$(DNF) --assumeyes copr
DNF_INSTALL=$(DNF) --assumeyes install
FISH=/usr/bin/fish
FISH_N=$(FISH) --no-execute
GIT=/usr/bin/git
INSTALL=/usr/bin/install
INSTALL_DATA=$(INSTALL) --mode=644 -D
INSTALL_DIR=$(INSTALL) --directory
INSTALL_PROGRAM=$(INSTALL) --mode=755 -D
MOCK=/usr/bin/mock
PYLINT=/usr/bin/pylint
PYTHON=/usr/bin/python3
PYTHON_SETUP=$(PYTHON) setup.py
RPM=/usr/bin/rpm
RPM_IMPORT=$(RPM) --import
RPMLINT=/usr/bin/rpmlint
SHELL=/bin/sh
SHELLCHECK=/usr/bin/shellcheck --shell=bash
SHELLCHECK_X=$(SHELLCHECK) -x
XZ=/usr/bin/xz
YAMLLINT=/usr/bin/yamllint

# Standard Makefile installation directories.
#
# The following are the standard GNU/RPM values which are defined in
# /usr/lib/rpm/macros. See
# http://www.gnu.org/software/make/manual/html_node/Directory-Variables.html
# for more information.
prefix=/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
datadir=$(prefix)/share
includedir=$(prefix)/include
infodir=$(prefix)/info
libdir=$(exec_prefix)/lib
libexecdir=$(exec_prefix)/libexec
localstatedir=$(prefix)/var
mandir=$(prefix)/man
rpmconfigdir=$(libdir)/rpm
sbindir=$(exec_prefix)/sbin
sharedstatedir=$(prefix)/com
sysconfdir=$(prefix)/etc

# Fedora installation directory overrides.
#
# We override some of the previous GNU/RPM default values with those
# values suiteable for a Fedora/RedHat/CentOS linux system, as defined
# in /usr/lib/rpm/redhat/macros.
#
# This section can be put into a separate file and included here if we
# want to create a more multi-platform Makefile system.
sysconfdir=/etc
defaultdocdir=/usr/share/doc
infodir=/usr/share/info
localstatedir=/var
mandir=/usr/share/man
sharedstatedir=$(localstatedir)/lib

# Makefile parameter variables
name:=pyinilint
version=$(shell awk '/Version:/ { print $$2 }' python-$(name).spec)
release=$(subst %{?dist},,$(shell awk '/Release:/ { print $$2 }' python-$(name).spec))
dist_name:=$(name)-$(version)
dist_tree-ish:=$(version)
tarball:=$(dist_name).tar.bz2
RELEASEVER:=33
fedora_dist:=$(release).fc$(RELEASEVER)
fedora_rpm_stem:=$(dist_name)-$(fedora_dist)
fedora_srpm:=python-$(fedora_rpm_stem).src.rpm
fedora_rpm:=$(fedora_rpm_stem).noarch.rpm
pyfedora_rpm:=python3-$(fedora_rpm_stem).noarch.rpm
fedora_mock_root:=fedora-$(RELEASEVER)-x86_64
mock_resultdir=.
testdestdir:=testdestdir
git_hooks_tgt_files:=$(patsubst git_hooks/%,.git/hooks/%,$(wildcard git_hooks/*))
requirements:=bzip2 git mock python3 python3-distro python3-rpm rpmlint ShellCheck xz yamllint

.PHONY: all
all: man/$(name).1.gz
	$(info all:)

man/$(name).1.gz: man/$(name).1
	gzip --best --stdout $< > $@

.PHONY: pybuild
pybuild: 
	$(PYTHON_SETUP) build

.PHONY: lint
lint:
	$(info lint:)
	$(PYLINT) $(name) setup.py
	$(SHELLCHECK) *.bash
	$(FISH_N) *.fish
	$(RPMLINT) *.spec
	$(YAMLLINT) .gitlab-ci.yml

.PHONY: test
test:
	$(info test:)
	$(PYTHON) -m unittest discover

.PHONY: testinstall
testinstall: | testdestdir install installcheck

testdestdir:
	mkdir testdestdir
	$(eval DESTDIR:=$(testdestdir))

.PHONY: install
install:
	$(info install:)
	$(INSTALL_DATA) $(name).completion.bash $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	$(INSTALL_DATA) $(name).completion.fish $(DESTDIR)$(datadir)/fish/completions/$(name).fish
	$(INSTALL_DATA) man/$(name).1.gz $(DESTDIR)$(mandir)/man1/$(name).1.gz

.PHONY: pyinstall
pyinstall: 
	$(info pyinstall:)
	$(PYTHON_SETUP) install

.PHONY: installcheck
installcheck:
	$(info installcheck:)
	test -r $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	test -r $(DESTDIR)$(datadir)/fish/completions/$(name).fish
	test -r $(DESTDIR)$(mandir)/man1/$(name).1.gz

.PHONY: installgithooks
installgithooks: $(git_hooks_tgt_files)
	$(info All git_hook have been installed in .git/hooks directory.)

$(git_hooks_tgt_files): git_hooks/*
	cp $< $@

.PHONY: dist
dist: $(tarball)

$(tarball):
	$(GIT) archive --format=tar --prefix=$(dist_name)/ $(dist_tree-ish) | $(BZIP2) > $(tarball)

.PHONY: srpm
srpm: $(fedora_srpm)

$(fedora_srpm): $(tarball)
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --buildsrpm \
		--spec python-$(name).spec --sources $(tarball)

.PHONY: rpm
rpm: $(fedora_srpm)
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --rebuild $(fedora_srpm)

.PHONY: pypisdistrelease
pypisdistrelease:
	$(PYTHON_SETUP) sdist
	$(PYTHON) -m twine upload dist/*

.PHONY: clean
clean:
	$(info clean:)
	find . -name '*.pyc' -delete
	find . -name '__pycache__' -delete
	rm -f *.rpm *.tar.bz2

.PHONY: distclean
distclean: clean
	$(info distclean:)
	find . -name '*~' -delete
	rm -f *.log man/$(name).1.gz
	rm -rf testdestdir build dist $(name).egg-info

.PHONY: requirements
requirements:
	sudo $(DNF_INSTALL) $(requirements)

.PHONY: help
help:
	$(info help:)
	$(info Usage: make TARGET [VAR1=VALUE VAR2=VALUE])
	$(info )
	$(info Targets:)
	$(info   all             The default target, redirects to build.)
	$(info   lint            Lint the source files.)
	$(info   testinstall     Perform a test installation.)
	$(info   install         Install.)
	$(info   installcheck    Post-installation check of all installed files.)
	$(info   installgithooks Install the git hooks in the local repository.)
	$(info   dist		 Create a distribution tarball.)
	$(info   srpm		 Create a SRPM.)
	$(info   rpm		 Create a RPM.)
	$(info   clean           Clean up all generated binary files.)
	$(info   distclean       Clean up all generated files.)
	$(info   help            Display this help message.)
	$(info   printvars       Print variable values (useful for debugging).)
	$(info   printmakevars   Print the Make variable values (useful for debugging).)
	$(info )

.PHONY: printvars
printvars:
	$(info printvars:)
	$(info DNF_INSTALL=$(DNF_INSTALL))
	$(info GIT=$(GIT))
	$(info INSTALL=$(INSTALL))
	$(info INSTALL_DATA=$(INSTALL_DATA))
	$(info INSTALL_DIR=$(INSTALL_DIR))
	$(info INSTALL_PROGRAM=$(INSTALL_PROGRAM))
	$(info MOCK=$(MOCK))
	$(info RPM=$(RPM))
	$(info RPM_IMPORT=$(RPM_IMPORT))
	$(info RPMLINT=$(RPMLINT))
	$(info SHELL=$(SHELL))
	$(info SHELLCHECK=$(SHELLCHECK))
	$(info SHELLCHECK_X=$(SHELLCHECK_X))
	$(info XZ=$(XZ))
	$(info prefix=$(prefix))
	$(info exec_prefix=$(exec_prefix))
	$(info bindir=$(bindir))
	$(info datadir=$(datadir))
	$(info includedir=$(includedir))
	$(info infodir=$(infodir))
	$(info libdir=$(libdir))
	$(info libexecdir=$(libexecdir))
	$(info localstatedir=$(localstatedir))
	$(info mandir=$(mandir))
	$(info rpmconfigdir=$(rpmconfigdir))
	$(info sbindir=$(sbindir))
	$(info sharedstatedir=$(sharedstatedir))
	$(info sysconfdir=$(sysconfdir))
	$(info name=$(name))
	$(info version=$(version))
	$(info release=$(release))
	$(info dist_name=$(dist_name))
	$(info dist_tree-ish=$(dist_tree-ish))
	$(info tarball=$(tarball))
	$(info RELEASEVER=$(RELEASEVER))
	$(info fedora_dist=$(fedora_dist))
	$(info fedora_rpm_stem=$(fedora_rpm_stem))
	$(info fedora_srpm=$(fedora_srpm))
	$(info fedora_rpm=$(fedora_rpm))
	$(info pyfedora_rpm=$(pyfedora_rpm))
	$(info fedora_mock_root=$(fedora_mock_root))
	$(info mock_resultdir=$(mock_resultdir))
	$(info testdestdir=$(testdestdir))
	$(info git_hooks_tgt_files=$(git_hooks_tgt_files))
	$(info requirements=$(requirements))

.PHONY: printmakevars
printmakevars:
	$(info printmakevars:)
	$(info $(.VARIABLES))
