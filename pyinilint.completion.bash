# Bash Completion scriptlet for pyinilint
#
# Author: Daniel J. R. May <daniel.may@danieljrmay.com>
# Version: 0.17

function _pyinilint()
{
    # shellcheck disable=SC2034  # words and cword are defined by _init_completion
    local cur prev words cword
    _init_completion || return
    
    local options='-b --basic -d --debug -e --encoding -h --help -i --interpolate -m --merge -o --output -r --raw -s --serialize -v --verbose'
    
    case $prev in
	pyinilint)
	    mapfile -t COMPREPLY < <(compgen -W "$options" -- "$cur")
	    _filedir
	    return
	    ;;	
	-e|--encoding)
	    local encodings
	    encodings=$(python3 -c 'from encodings.aliases import aliases; s = set(aliases.values()); print(" ".join(sorted(s)))')
	    mapfile -t COMPREPLY < <(compgen -W "$encodings" -- "$cur")
	    return
	    ;;
	-h|--help)
	    return
	    ;;
    esac

    if [[ $cur = -* ]]
    then
	mapfile -t COMPREPLY < <(compgen -W "$options" -- "$cur")
	return
    fi

    _filedir
}
complete -F _pyinilint pyinilint
