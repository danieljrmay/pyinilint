# Create Virtual Environment

You can install, activate and configure a virtual environment by
executing the following commands in your Bash shell.

```sh
python3 -m venv pyinilint-venv
source pyinilint-venv/bin/activate
pip install --upgrade pip
pip install rope jedi flake8 autopep8 yapf
```

This simply installs some packages which are useful for development.

* **rope**: a refactoring library for Python.
* **jedi**: autocompletion library for Python.
* **flake8**: style guide enforcement for Python.
* **autopep8**: source code formatting for Python.
* **yapf**: another formatter for Python.

**pyinilint** does not require anything which is not already in the
Python standard libraries.

You can deactivate the virtual environment with:

```sh
deactivate
```
