#
# Completions fo the pyinilint command
#

complete -c pyinilint -s h -l help -d "Show help" -f

set -l encodings (python3 -c 'from encodings.aliases import aliases; s = set(aliases.values()); print(" ".join(sorted(s)))')
complete -c pyinilint -s e -l encoding -x -k -a "$encodings" -d "Set character encoding"

complete -c pyinilint -s b -l basic  -d "Basic interpolation"
complete -c pyinilint -s d -l debug  -d "Show debug messages"
complete -c pyinilint -s i -l interpolate -d "Interpolate but no output"
complete -c pyinilint -s m -l merge -d "Merge files"
complete -c pyinilint -s o -l output -d "Output to stdout"
complete -c pyinilint -s r -l raw -d "Raw output, no interpolation"
complete -c pyinilint -s s -l serialize -d "Output interpolated to stdout"
complete -c pyinilint -s v -l verbose -d "Show verbose messages"
