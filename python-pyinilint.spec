%global srcname pyinilint

Name:      python-%{srcname}
Version:   0.17
Release:   1%{?dist}
Summary:   Linter for Python INI-like configuration files

License:   GPLv3
URL:       https://gitlab.com/danieljrmay/%{srcname}
Source0:   %{url}/-/archive/%{version}/%{srcname}-%{version}.tar.bz2

BuildArch: noarch
BuildRequires: make python3-setuptools

%description
A linter for INI-like configuration files which are intended to be
parsed by the standard Python Lib/configparser.py module.


%package -n python3-%{srcname}
Summary:       Python 3 build of the pyinilint module
BuildRequires: python3-devel
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
The Python 3 module which provides the core functionality for the
pyinilint tool.


%package -n %{srcname}
Summary:  %{summary}
Requires: python3-%{srcname} = %{version}-%{release}

%description -n %{srcname}
A linter for INI-like configuration files which are intended to be
parsed by the standard Python Lib/configparser.py module.


%package -n %{srcname}-fish
Summary:  Fish shell completions scripts for pyinilint
Requires: fish %{srcname} = %{version}-%{release}

%description -n %{srcname}-fish
Fish shell completions for pyinilint.


%prep
%autosetup -n %{srcname}-%{version}


%build
%py3_build
%make_build


%install
%py3_install
%make_install


%check
%{__python3} setup.py test


%files -n python3-%{srcname}
%doc AUTHORS README.md
%license LICENSE.txt
%{python3_sitelib}/*


%files -n %{srcname}
%doc AUTHORS README.md
%license LICENSE.txt
%{_bindir}/%{srcname}
%{_datadir}/bash-completion/completions/%{srcname}
%{_mandir}/man1/%{srcname}.1.gz


%files -n %{srcname}-fish
%{_datadir}/fish/completions/%{srcname}.fish


%changelog
* Wed Feb 23 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.17-1
- Synchronize version numbers.
- Remove TODO file, this is handled by GitLab issues.

* Fri Feb 18 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.16-1
- Removing image from release and changing documentaiton to use GitLab URL.

* Fri Feb 18 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.15-1
- Add logo image to release.

* Fri Feb 18 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.14-1
- Fixing version numbers so everthing is in sync.

* Thu Nov 19 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.12-1
- Add fish shell completions 
- Add missing --interpolate section to options.
- Fix lints and add virtual environment documentation.

* Mon Mar 11 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.11-1
- Updated to work with flycheck-ini-pyinilint.

* Wed Jan 23 2019 Daniel J. R. May <daniel.may@kada-media.com> - 0.10-1
- Updated documentation.

* Fri Jan 18 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.9-1
- Add version information to help.

* Fri Jan 11 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.8-1
- Update documentation.

* Fri Jan 11 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.7-1
- Add serialize option.

* Thu Jan 10 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.6-1
- Updated documentation.
- Fixed exit status codes.

* Wed Jan  9 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.5-1
- Updated packaging.

* Tue Jan  8 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.4-1
- Added tests.

* Thu Dec 20 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.3-1
- Upstream release.

* Thu Dec 20 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2-1
- Initial release of refactored pyinilint.
